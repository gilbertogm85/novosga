<?php

namespace Novosga\Model\Util;

use Novosga\Model\Model;

/**
 * Classe auxiliar.
 *
 * @author rogerio
 */
class Cliente extends Model
{
    private $nome;
    private $documento;
    private $exame;
    private $empresa;

    public function __construct()
    {
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    public function getEmpresa()
    {
        return $this->empresa;
    }

    public function setExame($exame)
    {
        $this->exame = $exame;
    }

    public function getExame()
    {
        return $this->exame;
    }

    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    public function getDocumento()
    {
        return $this->documento;
    }

    public function toString()
    {
        return $this->getNome();
    }
}
